﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GL.UI
{
    public class UIManager : MonoBehaviour
    {
        /********************************************* Variables *********************************************/
        public static UIManager Instance { get; private set; }

        public delegate void OnStartGameDelegate();
        public static event OnStartGameDelegate onStartGame;

        public delegate void OnStopGameDelegate();
        public static event OnStopGameDelegate onStopGame;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Awake()
        {
            Instance = this;
        }
        /****************************************** Public Methods ********************************************/

        public void CallOnStopGame()
        {
            onStopGame.Invoke();
        }
        public void CallOnStartGame()
        {
            onStartGame.Invoke();
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
