﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GL.UI
{
    public class GameHUdScreen : ScreenBase
    {
        /********************************************* Variables *********************************************/
        public Button startButton;
        public Button stopButton;



        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void OnEnable()
        {
            startButton.onClick.AddListener(OnStartClick);
            stopButton.onClick.AddListener(OnStopClick);
        }

        private void OnDisable()
        {
            startButton.onClick.RemoveListener(OnStartClick);
            stopButton.onClick.RemoveListener(OnStopClick);
        }
        /****************************************** Public Methods ********************************************/
        public void OnStartClick()
        {
            UIManager.Instance.CallOnStartGame();

            startButton.gameObject.SetActive(false);
            stopButton.gameObject.SetActive(true);
        }
        public void OnStopClick()
        {
            UIManager.Instance.CallOnStopGame();

            stopButton.gameObject.SetActive(false);
            startButton.gameObject.SetActive(true);
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
