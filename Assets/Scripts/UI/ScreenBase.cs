﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GL.UI
{
	public class ScreenBase : MonoBehaviour
	{
		public virtual bool IsActive { get; private set; }

		/// <summary>
		/// Initialize the Panel
		/// </summary>
		public virtual void Init()
		{
		}

		/// <summary>
		/// Activate the Panel
		/// </summary>
		public virtual void Activate()
		{
			//			Debug.Log ("Activate - " + GetType ().Name);
			gameObject.SetActive(true);
			IsActive = true;
		}

		/// <summary>
		/// Deactivate the Panel
		/// </summary>
		public virtual void Deactivate()
		{
			gameObject.SetActive(false);
			IsActive = false;
		}



	}
}

	
