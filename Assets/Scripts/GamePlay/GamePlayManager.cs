﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GL.UI;

namespace GL.gameplay
{


    public class GamePlayManager : MonoBehaviour
    {
        /********************************************* Variables *********************************************/

        public static GamePlayManager Instance { get; private set; }
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            UIManager.onStartGame += OnStartGame;
            UIManager.onStopGame += OnStopGame;
        }
        private void OnDisable()
        {
            UIManager.onStartGame -= OnStartGame;
            UIManager.onStopGame -= OnStopGame;
        }
        /****************************************** Public Methods ********************************************/

        private void Start()
        {
            GridController.Instance.CreateGrid();
        }
        private void OnStartGame()
        {
            GridController.Instance.StartGeneration();
        }

        private void OnStopGame()
        {
            GridController.Instance.StopGeneration();
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
