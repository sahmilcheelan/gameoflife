﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GL.common;
using UnityEngine.UI;
namespace GL.gameplay
{

    [System.Serializable]
    public class CellItem : MonoBehaviour
    {

        /********************************************* Variables *********************************************/

        public Image cellImage;

        private Enum.CellStates _state;

        public Enum.CellStates State
        {
            get { return _state; }
            private set { _state = value; }
        }

        private Enum.CellStates _upComingState;
        private Button _cellItemButton;
        public Enum.CellStates UpComingState
        {
            get { return _upComingState; }
            private set { _upComingState = value; }
        }
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/

        private void OnEnable()
        {
            Debug.Log("sahmil -- on enable cell item");
            State = Enum.CellStates.dead;
            _cellItemButton = this.gameObject.GetComponent<Button>();
            _cellItemButton.onClick.AddListener(ToggleCellState);
        }
        private void OnDisable()
        {
            _cellItemButton.onClick.RemoveListener(ToggleCellState);
        }
        /****************************************** Public Methods ********************************************/






        /// <summary>
        /// Update the cell state
        /// </summary>
        public void UpdateCellState()
        {
            State = UpComingState;
            EnableCellImage();
        }

        /// <summary>
        /// set the upcoming state to be updated
        /// </summary>
        /// <param name="newState"></param>
        public void SetUpcomingCellState(Enum.CellStates newState)
        {
            UpComingState = newState;
           
        }

        public void OnCellClick()
        {
            ToggleCellState();
        }

        /// <summary>
        /// toggle the current cell state on clicc
        /// </summary>
        private void ToggleCellState()
        {
            State = (State == Enum.CellStates.alive ? Enum.CellStates.dead :  Enum.CellStates.alive);
              Debug.Log("sahmil -- on button click");
            EnableCellImage();


        }

        private void EnableCellImage()
        {
            Debug.Log("sahmil -- on enable cell image");
            cellImage.enabled = (State == Enum.CellStates.alive ? true : false);
        }

        public void ResetCell()
        {
            State = Enum.CellStates.dead;
            EnableCellImage();
            SetButtonInteractable(true);
        }

        public void SetButtonInteractable(bool toggle)
        {
            _cellItemButton.interactable = toggle;

        }
        /****************************************** Unity Calls & Events **************************************/

    }

}
