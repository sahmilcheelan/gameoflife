﻿using System.Collections;
using System.Collections.Generic;
using GL.common;
using UnityEngine;

namespace GL.gameplay
{
    [System.Serializable]
    public class GridController : MonoBehaviour
    {
        /********************************************* Variables *********************************************/

        public CellItem cellItemPrefab;
        public GameObject gridParent;

        //private List<List<CellItem>> grid;

        public CellItem[,] grid;

        public static GridController Instance { get; private set; }

        private int _rows;
        private int _cols;

        private List<CellItem> updatedCellList= new List<CellItem>();
        private bool hasGameStarted;
        
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Awake()
        {
            Instance = this;
        }
        /****************************************** Public Methods ********************************************/
        [ContextMenu("debugCreateGrid")]
        public void StartGeneration()
        {
            hasGameStarted = true;
            for (int row = 0; row < _rows; row++)
            {
                for (int col = 0; col < _cols; col++)
                {
                  
                        grid[row, col].SetButtonInteractable(false);
                        updatedCellList.Add(grid[row, col]);
                
                }
            }
            StartCoroutine(StartGenerationCoroutine());
        }

        [ContextMenu("debugStop")]
        public void StopGeneration()
        {
            StopCoroutine(StartGenerationCoroutine());
            hasGameStarted = false;
            ResetGrid();

        }
        private IEnumerator StartGenerationCoroutine()
        {
            while(hasGameStarted)
            {

            PopulateCells();
            yield return  new WaitForSeconds(0.5f);

            }
        }

        /// <summary>
        /// creates the grid based on the number of rows and cols provided
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        public void CreateGrid(int rows = 6, int cols = 6)
        {
            _rows = rows;
            _cols = cols;
            grid = new CellItem[rows, cols];
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    var v_Cell = Instantiate(cellItemPrefab);
                    v_Cell.transform.SetParent(gridParent.transform);
                    v_Cell.transform.localScale = Vector3.one;
                    grid[row, col] = v_Cell;
                }
            }
        }
     
        public void PopulateCells()
        {

            updatedCellList.Clear();
            for (int row = 0; row < _rows; row++)
            {
                for (int col = 0; col < _cols; col++)
                {
                    var v_currentState = grid[row, col].State;
                   var v_neighBoursList = GetNeighBoursList(row, col);
                    var v_aliveNeighboursCount = GetNeighboursStateCount(v_neighBoursList, Enum.CellStates.alive);
                 
                    var v_upcomingState = GetUpcomingState(v_aliveNeighboursCount, grid[row, col].State);
                    if(v_currentState!=v_upcomingState)
                    {
                        grid[row, col].SetUpcomingCellState(v_upcomingState);
                        updatedCellList.Add(grid[row, col]);
                    }
                }
            }

           foreach(var v_cell in updatedCellList)
            {
                v_cell.UpdateCellState();
                Debug.Log("updated list");
            }
        }

        /// <summary>
        /// returns the list of neighBour Cells. neighbour is counted considering the grid as a circular plane
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        private  List<CellItem> GetNeighBoursList(int i, int j)
        {
            var v_prevRow = 0;
            var v_prevCol = 0;

            var v_curRow = 0;
            var v_curCol = 0;

            var v_nextRow = 0;
            var v_nextCol = 0;

            if (i == 0)
            {
                v_prevRow = _rows - 1;

            }
            else
            {
                v_prevRow = i - 1;
            }
            if (i == _rows - 1)
            {
                v_nextRow = 0;
            }
            else
            {
                v_nextRow = i + 1;
            }
            v_curRow = i;


            if (j == 0)
            {
                v_prevCol = _cols - 1;

            }
            else
            {
                v_prevCol = j - 1;
            }
            if (j == _cols - 1)
            {
                v_nextCol = 0;
            }
            else
            {
                v_nextCol = j + 1;
            }
            v_curCol = j;

            var v_neigbourCellsList = new List<CellItem>();

            v_neigbourCellsList.Add(grid[v_curRow, v_prevCol]);
            v_neigbourCellsList.Add(grid[v_curRow, v_nextCol]);

            v_neigbourCellsList.Add(grid[v_prevRow, v_curCol]);
            v_neigbourCellsList.Add(grid[v_nextRow, v_curCol]);

            v_neigbourCellsList.Add(grid[v_prevRow, v_prevCol]);
            v_neigbourCellsList.Add(grid[v_nextRow, v_nextCol]);

            v_neigbourCellsList.Add(grid[v_prevRow, v_nextCol]);
            v_neigbourCellsList.Add(grid[v_nextRow, v_prevCol]);

           
            return v_neigbourCellsList;
        }


        /// <summary>
        /// returns the neigbours count based on its current state
        /// </summary>
        /// <param name="neighhBourList"></param>
        /// <param name="State">the state for which you want the count</param>
        /// <returns></returns>
        private int GetNeighboursStateCount(List<CellItem> neighhBourList, Enum.CellStates State)
        {
            var v_count = 0;
            foreach(var v_cell in neighhBourList)
            {
                
                if (v_cell.State == State)
                    v_count++;
               
            }
            return v_count;
        }

        /// <summary>
        /// returns the upcomong state of the cell based on the rules of game of life
        /// </summary>
        /// <param name="aliveNeighboursCount"></param>
        /// <param name="currentCellState"></param>
        /// <returns></returns>
        private Enum.CellStates GetUpcomingState(int aliveNeighboursCount, Enum.CellStates currentCellState)
        {

            if(currentCellState == Enum.CellStates.alive)
            {
                if(aliveNeighboursCount == 2 || aliveNeighboursCount == 3)
                {
                    return Enum.CellStates.alive;
                }
                else
                {
                    return Enum.CellStates.dead;
                }
            }
            else
            {
                if(aliveNeighboursCount == 3)
                {
                    return Enum.CellStates.alive;
                }
                else
                {
                    return Enum.CellStates.dead;
                }
            }
          
        }

        /// <summary>
        /// to reset all the cells to dead state and makes the cells intecractable
        /// </summary>
        private void ResetGrid()
        {


            for (int row = 0; row < _rows; row++)
            {
                for (int col = 0; col < _cols; col++)
                {
                    grid[row, col].ResetCell();
                }
            }
        }
        /****************************************** Unity Calls & Events **************************************/


    }
}
